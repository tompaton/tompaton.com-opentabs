.PHONY: build up deploy test mypy coverage


build:
	docker-compose build


up:
	docker-compose up


deploy:
	docker push registry.tompaton.com/tompaton/opentabs
	ssh phosphorus "docker pull registry.tompaton.com/tompaton/opentabs"
	scp production.* phosphorus:/var/data/opentabs.tompaton.com/
	# docker-compose -f production.yml up -d


test:
	docker-compose run --rm opentabs -mpytest


mypy:
	docker-compose run --rm --entrypoint mypy opentabs -p opentabs \
	  --ignore-missing-imports --strict


coverage:
	docker-compose run --rm opentabs -mpytest \
	  --cov=opentabs --cov-report=html:/src/opentabs/htmlcov --cov-branch
	python3 -m http.server --directory opentabs/htmlcov/
