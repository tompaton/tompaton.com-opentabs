from waitress import serve

from opentabs.opentabs import app

if __name__ == '__main__':
    serve(app, port='5000')
