import os
import sqlite3
from calendar import Calendar, month_name
from dataclasses import dataclass
from datetime import datetime, date, timedelta
from flask import Flask
from flask import jsonify
from flask import request
from flask import redirect
from flask import url_for, Response as flask_Response
from flask_httpauth import HTTPBasicAuth
from mako.template import Template

from typing import Dict, List, Optional, Tuple, Union, cast


Year = int
Month = int
Day = int


Response = Union[str, flask_Response]


app = Flask(__name__)
auth = HTTPBasicAuth()


@auth.get_password  # type: ignore
def get_password(username: str) -> Optional[str]:
    if username == os.environ.get('USERNAME'):
        return os.environ.get('PASSWORD')
    return None


@app.route('/', methods=['GET'])
def get_opentabs() -> Response:
    now = datetime.now()
    with get_conn() as conn:
        open_urls = get_open_urls(conn)
    if request.accept_mimetypes.accept_html:
        return cast(Response,
                    Template(filename="opentabs/template.html")
                    .render_unicode(urls=group_urls(open_urls),
                                    now=now,
                                    mobile_css=open("opentabs/mobile.css").read()))
    else:
        return cast(Response,
                    jsonify([{'url_id': row['url_id'],
                              'url': row['url'],
                              'title': row['title']}
                             for row in open_urls]))


@app.route('/', methods=['POST'])
@auth.login_required  # type: ignore
def save_url() -> Response:
    url = request.form['page_url']
    title = request.form['page_title']
    with get_conn() as conn:
        url_id = get_url_id(conn, url, title)
        open_url(conn, url_id, datetime.now())
        conn.commit()
    if request.accept_mimetypes.accept_html:
        return cast(Response,
                    redirect(url_for('get_opentabs')))
    else:
        return cast(Response,
                    jsonify({'url_id': url_id, 'url': url, 'title': title}))


@app.route('/<int:url_id>', methods=['DELETE'])
@auth.login_required  # type: ignore
def delete_url(url_id: int) -> str:
    with get_conn() as conn:
        close_url(conn, url_id, datetime.now())
        conn.commit()
    return 'true'


@app.route('/history/', methods=['GET'])
@auth.login_required  # type: ignore
def get_history_page() -> Response:
    with get_conn() as conn:
        history = get_history(conn)

    return cast(Response,
                Template(filename="opentabs/history.html")
                .render_unicode(
                    calendar=History(history).get_calendar()))


@app.route('/history/<int:year>-<int:month>-<int:day>/', methods=['GET'])
@auth.login_required  # type: ignore
def get_history_detail_page(year: Year, month: Month, day: Day) -> Response:
    page_date = f'{year}-{month:02}-{day:02}'
    prev_date = date(year, month, day) - timedelta(days=1)
    next_date = date(year, month, day) + timedelta(days=1)

    with get_conn() as conn:
        opened = get_history_detail(conn, page_date, 'opened')
        closed = get_history_detail(conn, page_date, 'closed')

    return cast(Response,
                Template(filename="opentabs/history_detail.html")
                .render_unicode(date=page_date,
                                prev_date=prev_date.strftime('%Y-%m-%d'),
                                next_date=next_date.strftime('%Y-%m-%d'),
                                urls=[('Opened', opened),
                                      ('Closed', closed)],
                                mobile_css=open("opentabs/mobile.css").read()))


@app.route('/search/<string:query>', methods=['GET'])
@auth.login_required  # type: ignore
def search_opentabs(query: str) -> Response:
    now = datetime.now()
    with get_conn() as conn:
        open_urls = get_history_search(conn, query)
    return cast(Response,
                Template(filename="opentabs/template.html")
                .render_unicode(urls=group_urls(open_urls),
                                now=now,
                                mobile_css=open("opentabs/mobile.css").read()))


DB = sqlite3.Connection
UrlRow = sqlite3.Row
UrlRows = List[UrlRow]


def get_conn() -> DB:
    conn = sqlite3.connect('/data/opentabs.db')
    conn.row_factory = UrlRow
    conn.execute("CREATE TABLE IF NOT EXISTS urls "
                 "(url text, title text, "
                 "UNIQUE (url))")
    conn.execute("CREATE TABLE IF NOT EXISTS history "
                 "(url_id integer, opened integer, closed integer, "
                 "UNIQUE (url_id, opened))")
    return conn


def get_open_urls(conn: DB) -> UrlRows:
    cur = conn.cursor()
    cur.execute("SELECT url_id, url, title, "
                "cast(julianday('now') - julianday(opened) AS Integer) AS age "
                "FROM urls INNER JOIN history "
                "ON urls.rowid == history.url_id "
                "WHERE closed IS NULL")
    return cur.fetchall()


def get_url_id(conn: DB, page_url: str, page_title: str) -> int:
    cur = conn.cursor()
    cur.execute("SELECT rowid FROM urls WHERE url=?", (page_url,))
    row = cur.fetchone()
    if row is None:
        cur = conn.cursor()
        cur.execute("INSERT INTO urls (url, title) VALUES (?, ?)",
                    (page_url, page_title))
        return int(cur.lastrowid)
    else:
        return int(row[0])


def open_url(conn: DB, url_id: int, opened: datetime) -> None:
    conn.execute("INSERT INTO history (url_id, opened) VALUES (?, ?)",
                 (url_id, opened))


def close_url(conn: DB, url_id: int, closed: datetime) -> None:
    conn.execute("UPDATE history SET closed=? "
                 "WHERE url_id=? AND closed IS NULL",
                 (closed, url_id))


def group_urls(urls: UrlRows) -> List[Tuple[str, UrlRows]]:
    groups: Dict[str, UrlRows] = {}
    for row in urls:
        domain = row['url'].split('/')[2]
        groups.setdefault(domain, [])
        groups[domain].append(row)
    return list(groups.items())


HistoryRow = sqlite3.Row
HistoryRows = List[HistoryRow]


def get_history(conn: DB) -> HistoryRows:
    cur = conn.cursor()
    cur.execute("SELECT dates.date, opened.count, closed.count "
                "FROM "
                "(SELECT strftime('%Y-%m-%d', opened) AS date "
                " FROM history "
                " UNION SELECT strftime('%Y-%m-%d', closed) AS date "
                " FROM history) dates "
                "LEFT OUTER JOIN "
                "(SELECT strftime('%Y-%m-%d', opened) AS date, "
                " count(*) AS count "
                " FROM history GROUP BY date) opened "
                "ON dates.date = opened.date "
                "LEFT OUTER JOIN "
                "(SELECT strftime('%Y-%m-%d', closed) AS date, "
                " count(*) AS count "
                " FROM history GROUP BY date) closed "
                "ON dates.date = closed.date "
                "ORDER BY dates.date DESC")
    return cur.fetchall()


def get_history_detail(conn: DB, date: str, col: str) -> UrlRows:
    cur = conn.cursor()
    cur.execute("SELECT url, title "
                "FROM urls INNER JOIN history "
                "ON urls.rowid == history.url_id "
                f"WHERE strftime('%Y-%m-%d', {col}) = ?",
                (date,))
    return cur.fetchall()


def get_history_search(conn: DB, query: str) -> UrlRows:
    cur = conn.cursor()
    query = f'%{query}%'
    cur.execute("SELECT url_id, url, title, opened, closed, "
                "cast(julianday('now') - julianday(opened) AS Integer) AS age "
                "FROM urls INNER JOIN history "
                "ON urls.rowid == history.url_id "
                "WHERE url LIKE ? OR title LIKE ?"
                "ORDER BY opened DESC",
                (query, query))
    return cur.fetchall()


@dataclass
class DayCount:
    day: Day
    url: str
    opened: int
    closed: int


Week = List[DayCount]
Weeks = List[Week]


class MonthRow:
    def __init__(self, year: Year, month: Month, weeks: Weeks) -> None:
        self.month = month
        self.name: str = month_name[month][:3]
        self.weeks = weeks


@dataclass
class YearRow:
    year: Year
    months: List[MonthRow]


class History:
    def __init__(self, history: HistoryRows) -> None:
        self.opened = {date: opened
                       for date, opened, closed in history
                       if date}
        self.closed = {date: closed
                       for date, opened, closed in history
                       if date}

    def get_years(self) -> List[Year]:
        dates = list(self.opened.keys()) + list(self.closed.keys())
        years = {int(date.split('-')[0]) for date in dates}
        return list(range(min(years), max(years) + 1))

    def get_day(self, year: Year, month: Month, day: Day) -> DayCount:
        date = f'{year}-{month:02}-{day:02}'
        return DayCount(day,
                        f'/history/{date}/',
                        self.opened.get(date, 0),
                        self.closed.get(date, 0))

    def get_weeks(self, year: Year, month: Month) -> Weeks:
        c = Calendar()
        return [[self.get_day(year, month, day) for day in week]
                for week in c.monthdayscalendar(year, month)]

    def get_calendar(self) -> List[YearRow]:
        return [YearRow(year,
                        [MonthRow(year, month, self.get_weeks(year, month))
                         for month in reversed(range(1, 12 + 1))])
                for year in reversed(self.get_years())]
