from opentabs import opentabs
from typing import cast


def test_group_urls() -> None:
    domain1 = 'example.com'
    domain2 = 'other.org'
    row1 = cast(opentabs.UrlRow, {'url': 'https://' + domain1 + '/page1.html'})
    row2 = cast(opentabs.UrlRow, {'url': 'https://' + domain1 + '/page2.html'})
    row3 = cast(opentabs.UrlRow, {'url': 'https://' + domain2 + '/page3.html'})
    row4 = cast(opentabs.UrlRow, {'url': 'https://' + domain1 + '/page4.html'})
    groups = opentabs.group_urls([row1, row2, row3, row4])
    assert groups == [(domain1, [row1, row2, row4]),
                      (domain2, [row3])]
