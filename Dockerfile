FROM python:3.7

RUN mkdir -p /src/opentabs /data

WORKDIR /src

COPY requirements.txt .

RUN pip install --no-cache-dir -r requirements.txt

RUN useradd -u 1234 appuser
RUN chown -R appuser:appuser /src /data
USER appuser

COPY opentabs /src/opentabs/

COPY server.py /src
COPY devel_server.py /src

EXPOSE 5000

ENTRYPOINT ["python"]
CMD ["server.py"]
